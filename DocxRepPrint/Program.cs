﻿using Microsoft.Office.Interop.Word;
using Novacode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DocxRepPrint
{
    class Program
    {


        /// <summary>
        /// DocX - Pozwala tylko na zamianę tekstu i zapis z powrotem do docx ale za to nie wymaga zainstalowanego offica (worda).
        /// Interop.Word - Pozwala na zamianę tekstu i zapis do pdf. Jest troche toporne i wymaga Offica.
        /// 
        /// 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var inPath = @"C:\Users\michal\Documents\visual studio 2015\Projects\DocxRepPrint\DocxRepPrint\doc\test.docx";
            var outPath = @"C:\Users\michal\Documents\visual studio 2015\Projects\DocxRepPrint\DocxRepPrint\doc\out.docx";
            var out2Path = @"C:\Users\michal\Documents\visual studio 2015\Projects\DocxRepPrint\DocxRepPrint\doc\out.pdf";

            // ===== DocX =====
            using (DocX document = DocX.Load(inPath))
            {
                document.ReplaceText("[test_1]", "334785965");
                document.ReplaceText("[test_2]", "sds/478524");
                document.ReplaceText("[test_3]", "sds/233332");
                document.ReplaceText("[test_4]", "Halina Kiepska");
                document.ReplaceText("[test_10]", "UPA");
                document.ReplaceText("[test_5]", "Nowy znacznie dłuższy fragment tekstu!");
                document.SaveAs(outPath);
            }


            // ===== Interop.Word =====
            Convert(
                   inPath,
                   out2Path,
                   WdSaveFormat.wdFormatPDF);


        }

        private static void FindAndReplace(Microsoft.Office.Interop.Word.Application doc, object findText, object replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //execute find and replace
            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }


        public static void Convert(string input, string output, WdSaveFormat format)
        {
            // Create an instance of Word.exe
            Microsoft.Office.Interop.Word.Application oWord = new Microsoft.Office.Interop.Word.Application();

            // Make this instance of word invisible (Can still see it in the taskmgr).
            oWord.Visible = false;

            // Interop requires objects.
            object oMissing = System.Reflection.Missing.Value;
            object isVisible = true;
            object readOnly = false;
            object oInput = input;
            object oOutput = output;
            object oFormat = format;

            // Load a document into our instance of word.exe
            Microsoft.Office.Interop.Word.Document oDoc = oWord.Documents.Open(ref oInput, ref oMissing, ref readOnly, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref isVisible, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            // Make this document the active document.
            oDoc.Activate();

            // ========================
            //  Tutaj można zamieniać tak samo jak w bibliotece DocX. 
            // ========================
            FindAndReplace(oWord, "[test_1]", "334785965");
            FindAndReplace(oWord, "[test_2]", "sds/478524");
            FindAndReplace(oWord, "[test_3]", "sds/233332");
            FindAndReplace(oWord, "[test_4]", "Halina Kiepska");
            FindAndReplace(oWord, "[test_10]", "UPA");
            FindAndReplace(oWord, "[test_5]", "Nowy znacznie dłuższy fragment tekstu!");

            // Save this document in format.
            oDoc.SaveAs(ref oOutput, ref oFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            // Always close Word.exe.
            oWord.Quit(false, ref oMissing, ref oMissing);
        }

    }
}
